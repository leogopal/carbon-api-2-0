<?php

$file = 'results';

$csv = array_map('str_getcsv', file($file . '.csv'));

unset($csv[0]);

$csv = array_map(function($row){
    return $row[0];
}, $csv);

$intervals = [];

$i = 1;
while ($i <= 100) {
    $intervals[] = getPercentile($csv, $i);
    $i++;
}

file_put_contents($file . '.json', json_encode($intervals));
exit;


function getPercentile($array, $percentile)
{
    $percentile = min(100, max(0, $percentile));
    $array = array_values($array);
    sort($array);
    $index = ($percentile / 100) * (count($array) - 1);
    $fractionPart = $index - floor($index);
    $intPart = floor($index);

    $percentile = $array[$intPart];
    $percentile += ($fractionPart > 0) ? $fractionPart * ($array[$intPart + 1] - $array[$intPart]) : 0;

    return $percentile;
}
