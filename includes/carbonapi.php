<?php

class CarbonAPI {
    use URL;
    use Helpers;

    const KWG_PER_GB = 1.805;
    const RETURNING_VISITOR_PERCENTAGE = 0.75;
    const FIRST_TIME_VIEWING_PERCENTAGE = 0.25;
    const PERCENTAGE_OF_DATA_LOADED_ON_SUBSEQUENT_LOAD = 0.02;
    const CARBON_PER_KWG_GRID = 475;
    const CARBON_PER_KWG_RENEWABLE = 33.4;
    const PERCENTAGE_OF_ENERGY_IN_DATACENTER = 0.1008;
    const PERCENTAGE_OF_ENERGY_IN_TRANSMISSION_AND_END_USER = 0.8992;
    const CO2_GRAMS_TO_LITRES = 0.5562;

    private $database;

    public function __construct()
    {
        $this->database = new Database;

        // -----------------------------------------------------------------------------
        // Register routes
        // -----------------------------------------------------------------------------
        Flight::route('/', [$this, 'routeRoot']);
        Flight::route('/site', [$this, 'routeSite']);
        Flight::route('/website', [$this, 'routeWebsite']);
        Flight::route('/history', [$this, 'routeHistory']);
        Flight::route('/data', [$this, 'routeData']);
        Flight::route('/b', [$this, 'routeBadgeHTML']);

        // -----------------------------------------------------------------------------
        // Start the server
        // -----------------------------------------------------------------------------
        Flight::start();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Route handlers
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    public function routeRoot(): void
    {
        require 'views/home.php';
    }

    public function routeWebsite(): void
    {
        $url = (string) Flight::request()->query['url'];

        $url = self::normaliseURL($url);
        $url = self::urlHost($url);

        if($url === ''){
            // Send an error
            Flight::json([
                'error' => 'Invalid URL'
            ]);

            // Die
            exit;
        }

        $pages = $this->database->getTestedPages($url);

        $baseURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

        $pages = array_map(function($page) use($baseURL) {
            return $baseURL . '/history?url=' . $page;
        }, $pages);

        Flight::json($pages);

        exit;
    }

    public function routeHistory(): void
    {
        // Pull the URL from the query with Flight
        $url = (string) Flight::request()->query['url'];
        $page = (int) Flight::request()->query['page'];

        // Get the URL form the query string
        $url = self::normaliseURL($url);

        // Validate its a legit URL. The normalise function simply add http if there
        // isnt one as google needs it later on
        if ($url === '' || self::urlHostAndPath($url) === '') {

            // Send an error
            Flight::json([
                'error' => 'Invalid URL'
            ]);

            // Die
            exit;
        }

        $entries = $this->database->getLatestEntries($url, [
            'page' => $page
        ]);

        if(empty($entries)){
            Flight::json([
                'error' => 'Something went wrong'
            ]);

            exit;
        }

        $entries = array_map(function($entry) {
            $entry['statistics'] = self::getStatistics($entry['bytes']);

            return $entry;
        }, $entries);

        // Ship the result back to the browser
        Flight::json($entries);

        // Die
        exit;
    }

    public function routeSite(): void
    {
        // Pull the URL from the query with Flight
        $url = (string) Flight::request()->query['url'];

        // Get the URL form the query string
        $url = self::normaliseURL($url);

        // Validate its a legit URL. The normalise function simply add http if there
        // isnt one as google needs it later on
        if ($url === '' || self::urlHostAndPath($url) === '') {

            // Send an error
            Flight::json([
                'error' => 'Invalid URL'
            ]);

            // Die
            exit;
        }

        $entry = self::getEntry($url);

        if(empty($entry)){
            Flight::json([
                'error' => 'Something went wrong'
            ]);

            exit;
        }

        // Ship the result back to the browser
        Flight::json($entry);

        // Die
        exit;
    }

    public function routeData(): void
    {
        // Get the URL form the query string
        $bytes = (int) Flight::request()->query['bytes'];
        $green = (bool) Flight::request()->query['green'];

        // Validate its a legit URL
        if ($bytes === 0) {
            // Send an error
            Flight::json([
                'error' => 'bytes can not be 0'
            ]);

            // Die
            exit;
        }

        $statistics = self::getStatistics($bytes);

        $co2 = $green === true ? $statistics['co2']['renewable']['grams'] : $statistics['co2']['grid']['grams'];

        // Ship the result back to the browser
        Flight::json([
            'statistics' => $statistics,
            'cleanerThan' => self::cleanerThan($co2)
        ]);

        // Die
        exit;
    }

    public function routeBadgeHTML(): void
    {
        // Pull data from the url
        $url = (string) Flight::request()->query['url'];

        // Normalise the data
        $url = self::normaliseURL($url);

        // Set CORS headers as this endpoint will be request from browsers
        Flight::before('json', function () {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET');
            header('Access-Control-Allow-Headers: Content-Type');
        });

        if ($url === '' || self::urlHostAndPath($url) === '') {

            // Send an error
            Flight::json([
                'error' => 'Invalid URL',
                'url'   => self::urlHostAndPath($url)
            ]);

            Flight::response()->status(400)->send();

            // Die
            exit;

        }

        $entry = self::getEntry($url);

        if(empty($entry)){
            // Send an error
            Flight::json([
                'error' => 'no result for url',
                'url'   => self::urlHostAndPath($url)
            ]);

            Flight::response()->status(400)->send();

            exit;
        }

        // Pull the carbon emissions
        $carbon = $entry['green'] === true ? $entry['statistics']['co2']['renewable']['grams'] : $entry['statistics']['co2']['grid']['grams'];

        // Ship it to the browser
        Flight::json([
            'c' => round($carbon, 2),
            'p' => round($entry['cleanerThan'] * 100, 0),
            'url' => $url,
        ]);

        exit;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // General helper functions
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    private function getEntry(string $url): array
    {
        // Get the latest entry for the url
        $entry = $this->database->getLatestEntry($url);

        // Check if it is more than a day old
        if(empty($entry) || $entry['timestamp'] < strtotime('-1 day', time())) {
            // Get a new entry
            $entry = self::makeEntry($url);
        }

        if(empty($entry)){
            return [];
        }

        $entry['statistics'] = self::getStatistics($entry['bytes']);
        $entry['cleanerThan'] = self::cleanerThan($entry['co2']);

        unset($entry['co2']);

        return $entry;
    }

    private function makeEntry(string $url): array
    {
        // Setup the default parameters required for google
        $pageSpeedParameters = [
            'url' => self::normaliseURL($url)
        ];

        // Add the google page speed api key if it exists
        if(defined('CC_GPSAPI_KEY') && CC_GPSAPI_KEY !== '') {
            $pageSpeedParameters['key'] = CC_GPSAPI_KEY;
        }

        try {
            // Send all the urls we need to process off together
            $results = self::asyncRequests([
                'pagespeedapi' => 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?' . http_build_query($pageSpeedParameters),
                'greenweb' => 'http://api.thegreenwebfoundation.org/greencheck/' . self::urlHost($url),
            ]);

        } catch (\Exception $e) {
            // We could put in a note here about why it failed for the api to handle
            return [];
        }

        // Placeholder until the API is back up
        $green = (bool) $results['greenweb']->green;

        // Calc the transfer size
        $bytesTransfered = self::calculateTransferedBytes(
            $results['pagespeedapi']->lighthouseResult->audits->{'network-requests'}->details->items
        );

        // Calculate the statistics as we need the co2 emissions
        $statistics = self::getStatistics($bytesTransfered);

        // pull the co2 relative to the energy
        $co2 = $green === true ? $statistics['co2']['renewable']['grams'] : $statistics['co2']['grid']['grams'];

        // This returns a sanitised version of the entry
        $entry = $this->database->insert([
            'url'   => $url,
            'bytes' => $bytesTransfered,
            'green' => $green,
            'co2'   => $co2,
        ]);

        return $entry;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Helper functions for calculating emissions
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    private function getStatistics(string $bytes): array
    {
        $bytesAdjusted = self::adjustDataTransfer($bytes);
        $energy = self::energyConsumption($bytesAdjusted);
        $co2Grid = self::getCo2Grid($energy);
        $co2Renewable = self::getCo2Renewable($energy);

        return [
            'adjustedBytes' => $bytesAdjusted,
            'energy' => $energy,
            'co2' => [
                'grid' => [
                    'grams' => $co2Grid,
                    'litres'=> self::co2ToLitres($co2Grid)
                ],
                'renewable' => [
                    'grams' => $co2Renewable,
                    'litres' => self::co2ToLitres($co2Renewable)
                ]
            ]
        ];
    }

    private static function adjustDataTransfer(int $val): int
    {
        return ($val * self::RETURNING_VISITOR_PERCENTAGE) + (self::PERCENTAGE_OF_DATA_LOADED_ON_SUBSEQUENT_LOAD * $val * self::FIRST_TIME_VIEWING_PERCENTAGE);
    }

    private static function energyConsumption(int $bytes): float
    {
        return $bytes * (self::KWG_PER_GB / 1073741824);
    }

    private static function getCo2Grid(float $energy): float
    {
        return $energy * self::CARBON_PER_KWG_GRID;
    }

    private static function getCo2Renewable(float $energy): float
    {
        return (($energy * self::PERCENTAGE_OF_ENERGY_IN_DATACENTER) * self::CARBON_PER_KWG_RENEWABLE) + (($energy * self::PERCENTAGE_OF_ENERGY_IN_TRANSMISSION_AND_END_USER) * self::CARBON_PER_KWG_GRID);
    }

    private static function co2ToLitres(float $co2): float
    {
        return $co2 * self::CO2_GRAMS_TO_LITRES;
    }
}
