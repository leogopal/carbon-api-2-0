<?php

// -----------------------------------------------------------------------------
// https://doc.nette.org/en/3.0/database-core
// -----------------------------------------------------------------------------

class Database {
    private $database;

    public function insert(array $entry): array
    {
        // Ensure a connection to the database exists. If it doesnt, return early
        // as we have no database to work with.
        // Validate the entry contains the fields we need
        if($this->connect() !== true){
            // Add a negative id to suggest that the result was not added to the db
            $entry['id'] = -1;

            return $entry;
        }

        // Make sure the URL is as clean as it can be :)
        $entry['url'] = CarbonAPI::urlHostAndPath($entry['url']);

        // Insert the record
        $this->database->query('INSERT INTO record', $entry);

        // Add the database ID for this entry
        $entry['id'] = $this->database->getInsertId();

        // Add a timestamp of the entry time
        $entry['timestamp'] = time();

        return $entry;
    }

    public function getTestedPages(string $url)
    {
        // Ensure a connection to the database exists. If it doesnt, return early
        // as we have no database to work with
        if($this->connect() !== true){
            return [];
        }

        $url = $url . '%';

        $entries = $this->database->fetchAll('
            SELECT url
            FROM record
            WHERE url LIKE ?
            GROUP BY url
        ', $url);

        if(empty($entries)) {
            return [];
        }

        return array_map(function($entry) {
            return $entry->url;
        }, $entries);
    }

    public function getLatestEntries(string $url, array $options = []): array
    {
        $entriesPerPage = 10;

        if(!is_int($options['page'])){
            $options['page'] = 0;
        }

        // Ensure a connection to the database exists. If it doesnt, return early
        // as we have no database to work with
        if($this->connect() !== true){
            return [];
        }

        // Normalise the components of the URL before sending it to the database
        $url = CarbonAPI::urlHostAndPath($url);

        $offset = ($options['page'] - 1) * $entriesPerPage;

        if($offset < 0){
            $offset = 0;
        }

        // Fetch one
        $entries = $this->database->fetchAll('
            SELECT id, timestamp, bytes, green, co2
            FROM record
            WHERE url = ?
            ORDER BY id DESC
            LIMIT ?
            OFFSET ?
        ', $url, $entriesPerPage, $offset);

        if(empty($entries)) {
            return [];
        }

        return array_map(function($entry) use($url) {
            return self::normaliseEntry($entry, $url);
        }, $entries);
    }

    public function getLatestEntry(string $url): array
    {
        // Ensure a connection to the database exists. If it doesnt, return early
        // as we have no database to work with
        if($this->connect() !== true){
            return [];
        }

        // Normalise the components of the URL before sending it to the database
        $url = CarbonAPI::urlHostAndPath($url);

        // Fetch one
        $entry = $this->database->fetch('
            SELECT id, timestamp, bytes, green, co2
            FROM record
            WHERE url = ?
            ORDER BY id DESC
        ', $url);

        if(is_null($entry)) {
            return [];
        }

        return self::normaliseEntry($entry, $url);
    }

    private function connect(): bool
    {
        // Check if the DB object has already been established
        if($this->database instanceof Nette\Database\Connection){
            return true;
        }

        if(!(defined('CC_DATABASE_DSN') && defined('CC_DATABASE_USER') && defined('CC_DATABASE_PASSWORD'))){
            return false;
        }

        // Setup the database
        $this->database = new Nette\Database\Connection(
            CC_DATABASE_DSN,
            CC_DATABASE_USER,
            CC_DATABASE_PASSWORD
        );

        return true;
    }

    private static function normaliseEntry(object $entry, string $url): array
    {
        $entry = (array) $entry;
        $entry['url'] = $url;
        $entry['timestamp'] = $entry['timestamp']->getTimestamp();

        return $entry;
    }
}
