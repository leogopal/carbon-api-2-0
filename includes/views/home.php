<h1>Website Carbon API</h1>
<hr>
<section>
    <h2>How is your website impacting the planet?.</h2>
    <p>
        The internet consumes a lot of electricity. 416.2TWh per year to be precise. To give you some perspective, that’s more than the entire United Kingdom.
    </p>
    <p>
        From data centres to transmission networks to the devices that we hold in our hands, it is all consuming electricity, and in turn producing carbon emissions.
    </p>
    <p>
        For more information about how we calculate the carbon emissions, please visit <a href="https://www.websitecarbon.com/how-does-it-work/">https://www.websitecarbon.com/how-does-it-work/</a>
    </p>
</section>
<hr>
<section>
    <h2>What is the API</h2>
    <p>The API powers <a href="https://www.websitecarbon.com/">https://www.websitecarbon.com/</a>, in that it runs the tests that produces the results the website shows.</p>
    <p>The API is build with PHP, <a href="http://flightphp.com/">Flight</a>, <a href="https://developers.google.com/speed/docs/insights/v5/get-started">Google Page Speed Insights</a>, and the <a href="https://api.thegreenwebfoundation.org/">Green Web Foundation API</a></p>
    <hr>
</section>

<section>
    <h2>API Endpoints</h2>

    <ul>
        <li>
            <a href="#routeRoot">/</a>
        </li>
        <li>
            <a href="#routeSite">Site</a>
        </li>
        <li>
            <a href="#routeWebsite">Website</a>
        </li>
        <li>
            <a href="#routeHistory">History</a>
        </li>
        <li>
            <a href="#routeData">Data</a>
        </li>
        <li>
            <a href="#routeBadge">Badge</a>
        </li>
    </ul>

    <h3 id="routeRoot">
        /
    </h3>
    <p>The page you are viewing at the moment</p>
    <p>
        <a href="https://api.websitecarbon.com">
            https://api.websitecarbon.com
        </a>
    </p>

    <h3 id="routeSite">
        /site?url={xxx}
    </h3>
    <p>This endpoint accepts a URL parameter and will run a test in real time to calculate the carbon emissions generated per page view</p>
    <p>This endpoint is cached and will only test the same URL once every 24 hours</p>
    <p>
        <a href="https://api.websitecarbon.com/site?url=wholegraindigital.com">
            https://api.websitecarbon.com/site?url=wholegraindigital.com
        </a>
    </p>

    <h3 id="routeWebsite">
        /website?url={xxx}
    </h3>
    <p>
        This endpoint accepts a URL parameter, and returns an array of urls linking to the history page for all pages
    </p>
    <p>
        <a href="https://api.websitecarbon.com/website?url=wholegraindigital.com">
            https://api.websitecarbon.com/website?url=wholegraindigital.com
        </a>
    </p>

    <h3 id="routeHistory">
        /history?url={xxx}
    </h3>
    <p>
        This endpoint accepts a URL parameter and a page parameter and returns 10 results at a time
    </p>
    <p>
        <a href="https://api.websitecarbon.com/history?url=wholegraindigital.com">
            https://api.websitecarbon.com/history?url=wholegraindigital.com
        </a>
    </p>

    <h3 id="routeData">
        /data?bytes={xxx}&green={1/0}
    </h3>
    <p>An endpoint to calculate the emissions of a page by manually passing the bytes and whether or not it is powered by green hosting</p>
    <p>
        <a href="https://api.websitecarbon.com/data?bytes=1000&green=1">
            https://api.websitecarbon.com/data?bytes=1000&green=1
        </a>
    </p>

    <h3 id="routeBadge">
        /b?url={xxx}
    </h3>
    <p>
        An endpoint with refined data output powering the website carbon badge.
    </p>
    <p>
        <a href="https://api.websitecarbon.com/b?url=wholegraindigital.com">
            https://api.websitecarbon.com/b?url=wholegraindigital.com
        </a>
    </p>
    <p>
        For more information, visit <a href="https://wholegrain.gitlab.io/website-carbon-badges/">https://wholegrain.gitlab.io/website-carbon-badges/</a>
    </p>
</section>
